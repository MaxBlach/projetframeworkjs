import { createStore } from "vuex";

export default createStore({
  state: {
    user: [
      {
        id: 1,
        name: "Dupont",
        firstname: "Jean",
        email: "Dupont.Jean@yahoo.fr",
        password: "motdepasse",
      },
      {
        id: 2,
        name: "Duchemin",
        firstname: "René",
        email: "Duchemin.Rene@yahoo.fr",
        password: "motdepasse",
      },
    ],
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getUserPairId(state) {
      return state.user.filter((user) => user.id % 2 === 0);
    },
  },
  mutations: {},
  actions: {},
  modules: {},
});
