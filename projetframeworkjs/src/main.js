import { createApp } from "vue";
import App from "./App.vue";
import useVuelidate from "@vuelidate/core";
import router from "./router";
import store from "./store";

createApp(App).use(store).use(router).use(useVuelidate).mount("#app");
